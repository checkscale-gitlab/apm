package com.apm.apm.test;

import com.apm.apm.test.model.GetAllTestResponse;
import com.apm.apm.test.model.GetTestResponse;
import com.apm.apm.test.model.SaveTestRequest;
import com.apm.apm.test.model.UpdateTestRequest;

public interface TestService {

    GetAllTestResponse getAll();

    GetTestResponse get(String id);

    GetTestResponse save(SaveTestRequest request);

    GetTestResponse update(UpdateTestRequest request);

    void delete(String id);

}
