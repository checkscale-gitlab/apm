package com.apm.apm.test;

public class TestNotFoundException extends RuntimeException {

    public TestNotFoundException(String msg) {
        super(msg);
    }
}
