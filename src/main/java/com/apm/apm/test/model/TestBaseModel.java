package com.apm.apm.test.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TestBaseModel {

    private String text;
}
